const userService = require('./userService')

const registerController = async (req, res) => {
    try{
        const status = await userService.createUserDBService(req.body)
        if(status){
            res.send({"status":true , "message": "User created successfully"})
        } else {
            res.send({"status":false , "message": "User already exist"})
        }
    }catch(err){
        res.send({"status":false , "message": "error creating user"})
    }
}
const loginController = async (req, res) => {
    try{
        const result = await userService.loginUserDBService(req.body)
        if(result.status){
            res.send({"status":true , "message": result.msg})
        } else {
            res.send({"status":false , "message": result.msg})
        }
    }catch(err){
        res.send({"status":false , "message": err.msg})
    }
}

module.exports = { registerController , loginController}