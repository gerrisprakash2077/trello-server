const userModel=require("./userModel")
const key='8712654871khkujgkjgkg'
const encryptor= require('simple-encryptor')(key)

const createUserDBService = (userDetails) => {
    return new Promise((resolve,reject)=>{
        const userModelData = new userModel()
        userModelData.firstName = userDetails.firstName
        userModelData.lastName = userDetails.lastName
        userModelData.email = userDetails.email
        userModelData.password = userDetails.password
        const encrypted = encryptor.encrypt(userDetails.password)
        userModelData.password=encrypted
        userModel.findOne({"email":userDetails.email}).then((user)=>{
            if(user){
                resolve(false)
            }else{
                userModelData.save().then((data,err)=>{
                    if(err){
                        reject(false)
                    }else{
                        resolve(true)
                    }
                })
            }
        })
    })
}

const loginUserDBService = (userdetails) => {
    return new Promise((resolve,reject) => {
        userModel.findOne({"email":userdetails.email}).then((user)=>{
            if(user){
                if(user!=undefined && user!=null){
                    const decrypted=encryptor.decrypt(user.password)
                    if(decrypted == userdetails.password){
                        resolve({status:true,msg:"user Validated successfully"})
                    }else{
                        reject({status: false , msg : "invalid password"})
                    }
                }
            }else{
                reject({status: false , msg : "invalid data"})
            }
        })
    })
}

module.exports= { createUserDBService , loginUserDBService}