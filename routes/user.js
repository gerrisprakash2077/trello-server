const express = require("express");
const router=express.Router();

const userController=require("../userBasic/userController")

router.route("/signin").post(userController.loginController)
router.route("/signup").post(userController.registerController)

 module.exports=router