const express = require("express")
const app = express()
const dbConnection =require("./config/dbConnection")

dbConnection()

app.use(express.json())

//routes
// app.get('/',(req,res)=>{
//     res.send("pello world")
// })
// app.use('contact',require())
// app.
// app.use('/api/todo',require('./routes/todo'))
app.use("/api",require("./routes/user"))

app.listen(3000,()=>{
    console.log("Node API is running on port 3000");
})